(function() {
  function useAudioWidget() {
    /**
     * Custom Element representing an audio player with simple controls (play/pause and volume) and an ephemeral progress indicator.
    */
    class AudioWidget extends HTMLElement {

      constructor(audioEl) {
        super()

        // class properties need to be delcared inside a function
        this.audioEl = audioEl

        this.wrapper = undefined
        this.playCtl = undefined
        this.volumeCtl = undefined

        this.audioCtx = undefined
        this.srcNode = undefined
        this.gainNode = undefined
        this.analyser = undefined
        this.analyserData = undefined
        this.binSize = Math.pow(2, 10)
        this.binDuration = undefined
        this.lastPaint = 0
        this.m = 0 // log constant
        this.userVolume = 0.8
        this.isPlaying = false
        this.canvasPaused = true
        this.volRampTimerId = undefined

        this.initDom()
        this.initAudioEvents()
        this.initCanvasResizing()

        this.audioEl.load()
      }

      /**
       * Initializes the component DOM by
       * - initializing and attaching a shadow DOM with
       *   a clone of the component template
       * - storing references to elements we'll use later
       * - registering interaction event handlers
       */
      initDom() {
        // replace with template
        const template = document.getElementById('audio-widget-template')
        this.shadow = this.attachShadow({mode: 'open'})
        this.shadow.appendChild(template.content.cloneNode(true))

        // display track name
        const trackName = this.audioEl.getAttribute("alt")
        this.shadow.querySelectorAll('.track-name')[0].textContent = trackName

        // store elements
        this.wrapper = this.shadow.querySelectorAll('.wrapper')[0]
        this.playCtl = this.shadow.querySelectorAll('.btn--play-ctl')[0]
        this.canvas = this.shadow.querySelectorAll('canvas')[0]
        this.loadingProgressIndicator = this.shadow.querySelectorAll('.loading-progress-indicator')[0]

        // register button click handler
        this.playCtl.addEventListener('click', e => this.onPlayClick())

        // init volume momentary visibility
        // and register volume slider handler
        this.volumeCtl = this.shadow.querySelectorAll('.btn--volume-ctl')[0]
        const volUpd = e => {
          if (this.volumeCtl.classList.contains('visible'))
            this.setVolume(this.getVolumeFromEventPosition(e))
        }
        const volOpts = {
          callbacks: {
            onTouchStart: volUpd,
            onTouchMove: e => {
              volUpd(e)
              e.preventDefault()
            },
            onMouseDown: volUpd,
            onMouseMove: e => {
              if (e.buttons > 0 && this.volumeCtl.classList.contains('visible'))
                this.setVolume(this.getVolumeFromEventPosition(e))
            }
          }
        }
        this.enableMomentaryVisibilityClass(this.volumeCtl, volOpts)

        // init canvas momentary visibility
        const canvasOpts = { targetEl: this.canvas }
        this.enableMomentaryVisibilityClass(this.wrapper, canvasOpts)
      }

      /**
       * Initializes the canvas resize observer
       * Ensures the canvas size matches the host size, pixel-for-pixel
       */
      initCanvasResizing() {
        const resizeWithBoundingClientRect = () => {
          const rect = this.shadow.host.getBoundingClientRect()
          this.canvas.width = rect.width
          this.canvas.height = rect.height
        }

        if ('ResizeObserver' in window) {
          // use ResizeObserver if available
          const ro = new ResizeObserver(entries => {
            if (entries[0].borderBoxSize !== undefined) {
              const size = entries[0].borderBoxSize
              this.canvas.width = size.inlineSize
              this.canvas.height = size.blockSize
            } else {
              resizeWithBoundingClientRect()
            }
          })
          ro.observe(this.shadow.host)
        } else {
          // otherwise debounce window resize events
          let resizeTimerId
          window.addEventListener('resize', () => {
            if (resizeTimerId !== undefined)
              clearTimeout(resizeTimerId)

            resizeTimerId = window.setTimeout(() => {
              resizeWithBoundingClientRect()
            }, 200)
          })
        }
      }

      /**
       * Initializes event handlers for AudioElement events
       */
      initAudioEvents() {
        if (window.DEBUG) {
          // test audio events
          const audioEvents = [
            'abort',
            'canplay',
            'canplaythrough',
            'ended',
            'error',
            'loadeddata',
            'loadstart',
            'loadedmetadata',
            'playing',
            'progress',
            'stalled',
            'suspend',
            'waiting'
          ]
          audioEvents.forEach(ev => {
            this.audioEl.addEventListener(ev, e => console.log(e.type, this.audioEl.getAttribute('alt')))
          })
        }

        const setPaused = e => {
          this.wrapper.classList.add('paused')
          this.wrapper.classList.remove('loading')
          this.wrapper.classList.remove('playing')
          this.wrapper.classList.remove('error')
        }

        const setPlaying = e => {
          this.wrapper.classList.add('playing')
          this.wrapper.classList.remove('paused')
          this.wrapper.classList.remove('loading')
          this.wrapper.classList.remove('error')
        }

        const setLoading = e => {
          this.wrapper.classList.add('loading')
          this.wrapper.classList.remove('paused')
          this.wrapper.classList.remove('playing')
          this.wrapper.classList.remove('error')
        }

        const setError = e => {
          this.wrapper.classList.add('error')
          this.wrapper.classList.remove('paused')
          this.wrapper.classList.remove('playing')
          this.wrapper.classList.remove('loading')
          hideLoadingProgress(e)
        }

        const setPlayable = e => {
          if (e.target.paused)
            setPaused(e)
          else
            setPlaying(e)
        }

        const showLoadingProgress = e => {
          this.loadingProgressIndicator.classList.remove('hidden')
        }

        const hideLoadingProgress = e => {
          this.loadingProgressIndicator.classList.add('hidden')
        }

        // loading events
        this.audioEl.addEventListener('loadstart', setLoading)
        this.audioEl.addEventListener('canplay', setPlayable)
        this.audioEl.addEventListener('stalled', hideLoadingProgress)
        this.audioEl.addEventListener('canplaythrough', hideLoadingProgress)
        this.audioEl.addEventListener('waiting', setLoading)
        this.audioEl.addEventListener('error', setError)
        this.audioEl.addEventListener('progress', showLoadingProgress)
        this.audioEl.addEventListener('suspend', hideLoadingProgress)

        // playback events
        this.audioEl.addEventListener('play', setPlaying)
        this.audioEl.addEventListener('playing', setPlaying)
        this.audioEl.addEventListener('pause', setPaused)
        this.audioEl.addEventListener('ended', setPaused)
      }

      /**
       * Bootstraps our AudioContext. Needs to be done after user ixn.
       * AudioContext holds our old HTMLAudioElement.
       */
      initAudioContext() {
        const AudioContext = window.AudioContext || window.webkitAudioContext
        this.audioCtx = new AudioContext()
        this.srcNode = this.audioCtx.createMediaElementSource(this.audioEl)
        this.gainNode = this.audioCtx.createGain()
        this.srcNode.connect(this.gainNode)
        this.gainNode.connect(this.audioCtx.destination)

        this.gainNode.gain.value = this.userVolume

        // init analyser
        this.analyser = this.audioCtx.createAnalyser()
        this.analyser.minDecibels = -150
        this.analyser.maxDecibels = -50
        this.analyser.fftSize = this.binSize
        this.analyserData = new Uint8Array(this.analyser.frequencyBinCount)
        this.srcNode.connect(this.analyser)

        this.binDuration = this.binSize / this.audioCtx.sampleRate
        this.binWidth = this.audioCtx.sampleRate / this.analyser.frequencyBinCount
        this.m = 0
        for (let i = 0; i < (this.analyser.frequencyBinCount / 2); i++) {
          this.m += Math.log10(i * 0.9 + 1)
        }
      }

      /**
       * Call this on an element to add .visible momentarily
       * at the start of hover or touch events
       */
      enableMomentaryVisibilityClass(el, options = {durationSlow: 1500, durationFast: 200, callbacks: {}}) {
        // business
        const classEl = options.targetEl || el
        options.durationSlow = options.durationSlow || 1500
        options.durationFast = options.durationFast || 200
        options.callbacks = options.callbacks || {}

        // timer mgmt
        let timerId
        const cancelTimer = () => {
          if (timerId !== undefined) {
            window.clearTimeout(timerId)
            timerId = undefined
          }
        }

        const startTimer = duration => {
          cancelTimer()
          classEl.classList.add('visible')
          timerId = window.setTimeout(() => {
            classEl.classList.remove('visible')
          }, duration)
        }

        // event handlers
        // this is the order in which these events "typically" take place

        const onTouchStart = (e) => {
          if ('onTouchStart' in options.callbacks)
            options.callbacks.onTouchStart(e)
          cancelTimer()
          classEl.classList.add('visible')
        }

        const onTouchMove = (e) => {
          if ('onTouchMove' in options.callbacks)
            options.callbacks.onTouchMove(e)
          cancelTimer()
        }

        const onTouchEnd = (e) => {
          if ('onTouchEnd' in options.callbacks)
            options.callbacks.onTouchEnd(e)
          startTimer(options.durationSlow)
        }

        const onMouseDown = (e) => {
          if ('onMouseDown' in options.callbacks)
            options.callbacks.onMouseDown(e)
          cancelTimer()
          e.preventDefault()
        }

        const onMouseMove = (e) => {
          if ('onMouseMove' in options.callbacks)
            options.callbacks.onMouseMove(e)
          startTimer(options.durationSlow)
          e.preventDefault()
        }

        const onMouseUp = (e) => {
          if ('onMouseUp' in options.callbacks)
            options.callbacks.onMouseUp(e)
          startTimer(options.durationSlow)
        }

        // Special case to mimic :hover behaviour with delay
        const onMouseLeave = (e) => {
          if ('onMouseLeave' in options.callbacks)
            options.callbacks.onMouseLeave(e)
          startTimer(options.durationFast)
        }


        el.addEventListener('touchstart', e => onTouchStart(e))
        el.addEventListener('touchmove', e => onTouchMove(e))
        el.addEventListener('touchend', e => onTouchEnd(e))
        el.addEventListener('mousedown', e => onMouseDown(e))
        el.addEventListener('mousemove', e => onMouseMove(e))
        el.addEventListener('mouseup', e => onMouseUp())
        el.addEventListener('mouseleave', e => onMouseLeave())
      }

      getVolumeFromEventPosition(e) {
        let y = 0;

        if ('clientY' in e)
          y = e.clientY
        else if ('touches' in e)
          y = e.touches[0].clientY
        else
          throw new Error('Could not determine position of event', e)

        const rect = this.volumeCtl.getBoundingClientRect()
        const vol = 1 - (y - rect.top) / rect.height
        return Math.max(0, Math.min(1, vol))
      }

      setVolume(newVol) {
        this.volumeCtl.style.setProperty('--volume', `${newVol * 100}%`)
        this.userVolume = newVol
        this.rampVolume(newVol, 100)
      }

      /**
       * Smoothly transition to a new volume over ${duration} ms
       * WebAudioAPI's ramps have spotty performance so I guess we'll do it ourselves
       */
      rampVolume(newVol, duration = 500) {
        if (this.audioCtx === undefined)
          this.initAudioContext()

        return new Promise((res, rej) => {
          const curve = new Float32Array(2)
          curve[0] = this.gainNode.gain.value
          curve[1] = newVol
          try {
            this.gainNode.gain.setValueCurveAtTime(curve, this.audioCtx.currentTime,
              duration / 1000)
            window.setTimeout(() => res(), duration + 5)
          } catch (err) {
            // there are two scenarios here:
            // 1. an old curve is in progress and we are interrupting it, therefore
            //  our targetVal is the intended one.
            // 2. between waiting for the old curve to finish and trying again, 
            //  another curve has taken place, therefore our targetVal is not
            //  the intended one.
            // in practice it looks like 2. is rarely reached, and only in
            // circumstances where it doesn't make much difference
            //
            // ...so anyway, I started debouncing...
            const goingUp = newVol > this.gainNode.gain.value

            if (this.volRampTimerId !== undefined)
              window.clearTimeout(this.volRampTimerId)

            this.volRampTimerId = window.setTimeout(() => {
              this.volRampTimerId = undefined
              if (goingUp && this.gainNode.gain.value >= newVol ||
                !goingUp && this.gainNode.gain.value <= newVol) {
                return res()
              }
              this.rampVolume(newVol, duration)
            }, duration)
          }
        })
      }

      onPlayClick() {
        if (this.audioCtx === undefined)
          this.initAudioContext()

        if (this.audioEl.readyState < 2) {
          console.warn(`Attempted to play before ready; readiness: ${this.audioEl.readyState}; src: ${this.audioEl.currentSrc}`)
          return
        }

        this.audioEl.volume = 1

        if (this.audioEl.paused) {
          this.audioEl.play()
          this.rampVolume(this.userVolume)
          this.startCanvasDrawing()
        } else {
          // immediate feedback: show pause icon before ramp is complete
          this.wrapper.classList.remove('playing')
          this.wrapper.classList.add('paused')

          this.rampVolume(0).then(() => {
            this.audioEl.pause()
            this.pauseCanvasDrawing()
          })
        }
      }

      updateCanvasDrawing() {
        // only paint on frames that are further apart than one bin size
        // ie don't paint if the bin data hasn't changed
        if (this.audioCtx.currentTime - this.lastPaint >= this.binDuration) {
          const ctx = this.canvas.getContext('2d')
          ctx.clearRect(0, 0, this.canvas.width, this.canvas.height)

          const color = '#7c6f64'
          ctx.fillStyle = color

          const binSize = this.binSize / 2
          // const graphedHz = this.analyser.sampleRate / 2
          // const height = this.canvas.height / binSize
          this.analyser.getByteFrequencyData(this.analyserData)

          let y = this.canvas.height
          ctx.globalAlpha = 0
          for (let i = 0; i < binSize; i++) {
            ctx.globalAlpha = this.analyserData[i] / 255.0
            const h = Math.round(binSize / this.m * Math.log10((binSize - i) * 0.9 + 1))
            ctx.fillRect(0, y -= h, this.canvas.width, h)
          }

          this.lastPaint = this.audioCtx.currentTime
        }

        if (!this.canvasPaused)
          window.requestAnimationFrame(() => this.updateCanvasDrawing())
      }

      startCanvasDrawing() {
        this.canvasPaused = false
        window.requestAnimationFrame(() => this.updateCanvasDrawing())
      }

      pauseCanvasDrawing() {
        this.canvasPaused = true
      }
    }
    window.customElements.define("audio-widget", AudioWidget)

    /**
     * Get all <audio> from a dom element; returning them as an array
     */
    function getAudioEls(container) {
      const els = container.querySelectorAll("audio")
      const retval = []
      for (let i = 0; i < els.length; i++)
        retval.push(els[i])
      return retval
    }

    // Do the replacement
    getAudioEls(document).forEach(audioEl => {
      audioEl.replaceWith(new AudioWidget(audioEl))
    })
  }

  if ('customElements' in window)
    useAudioWidget()
})()
