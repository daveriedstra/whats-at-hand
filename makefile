# vim having trouble with tabs / spaces autocmd,
# use setlocal noexpandtab

tmp/%.js: src/%.js
	mkdir -p tmp
	gawk -i readfile '\
		! match($$0, /^\s*\{\{(.*)\}\}\s*$$/, refs) { print ; next }\
		{ printf "%s", readfile(refs[1]) }\
	' $< > $@

tmp/%.css: src/%.css
	mkdir -p tmp
	gawk -i readfile '\
		! match($$0, /^\s*\{\{(.*)\}\}\s*$$/, refs) { print ; next }\
		{ printf "%s", readfile(refs[1]) }\
	' $< > $@

dist/index.html: src/index.html tmp/audio-widget.css tmp/main.js tmp/inline-console.js tmp/style.css
	mkdir -p dist
	cat src/index.html |\
		sed -E 's/\{\{src(.*\.(js|css))/\{\{tmp\1/' |\
		gawk -i readfile '\
			! match($$0, /^\s*\{\{(.*)\}\}\s*$$/, refs) { print ; next }\
			{ printf "%s", readfile(refs[1]) }\
		' > dist/index.html

all: dist/index.html

.PHONY: all clean

clean:
	rm tmp/*
	rmdir tmp
